#!/usr/bin/env bash

set -euo pipefail

T_RED=$(tput setaf 1 || true)
T_GREEN=$(tput setaf 2 || true)
T_BOLD=$(tput bold || true)
T_RESET=$(tput sgr0 || true)

# Defaults. This is needed because of the bash set restriction
KEYID=${KEYID:-}
KEYID_OPT=${KEYID_OPT:-}

# Common values
GITLAB_CI_RELEASE_URL="https://gitlab.torproject.org/tpo/core/tor-ci-release"

function die()
{
    echo "${T_BOLD}${T_RED}FATAL ERROR:${T_RESET} $*" 1>&2
    exit 1
}

function runcmd()
{
    echo "${T_BOLD}${T_GREEN}\$ $*${T_RESET}"
    if ! "$@" ; then
        die "command '$*' has failed"
    fi
}

function usage()
{
    echo "$(basename "$0") [-h] [-k <gpg-keyid>]"
    echo
    echo "  arguments:"
    echo "   -h: show this help text"
    echo "   -k: Which GPG keyid to sign the tarballs with"
    echo
}

function download()
{
    local URL="$1"

    OPTS="-O"
    if [ -n "$2" ]; then
        OPTS="-o $2"
    fi

    # If "OPTS" are quoted, the filename ends up with an extra whitespace.
    # shellcheck disable=SC2086
    http_code=$(curl -L -w "%{http_code}" $OPTS "$URL")
    # shellcheck disable=SC2181
    if [ "$?" -ne 0 ] || [ "$http_code" -ne 200 ]; then
        die "Failed to download $URL. HTTP code: $http_code. Stopping"
    fi
}

while getopts "hk:" opt; do
    case "$opt" in
        h) usage
            exit 0
            ;;
        k) KEYID="$OPTARG"
            shift
            OPTIND=$((OPTIND - 1))
            ;;
        *)
            echo
            usage
            exit 1
            ;;
    esac
done

BUILD="$(pwd)/build"
SIGS_DIRPATH="$(pwd)/sigs"

# Cleanup previous build directory since we need to start from scratch. Prompt
# before removal just in case since rm -rf are scary.
if [ -d "$BUILD" ]; then
    runcmd rm -rf -I "$BUILD"
fi

# Create the directory hierarchy we need.
runcmd mkdir -p "$BUILD"
runcmd mkdir -p "$SIGS_DIRPATH"

# Get in the build directory and start the process.
runcmd cd "$BUILD"

# Get the Tor CI release repository to use to build tarball(s).
if [ ! -d "tor-ci-release" ]; then
    runcmd git clone "$GITLAB_CI_RELEASE_URL"
else
    runcmd git pull
fi
runcmd cd tor-ci-release/

# Fetch the version artifacts so we can learn which version and which branch
# to use to build the tarballs.
URL="$GITLAB_CI_RELEASE_URL/-/jobs/artifacts/main/download?job=validation"
download "$URL" "artifacts.zip"
runcmd unzip -o artifacts.zip

# Export these variables for the Tor CI release build script. We hijack those
# to point to what we want to use.
export VERSIONS_DIR="$BUILD/tor-ci-release/artifacts/versions"
export TARBALLS_DIR="$BUILD/tarballs"
# This must be set else our bash set restrictions do not like it.
export BUILDDIR="/"

# Create the tarball directory.
runcmd mkdir -p "$TARBALLS_DIR"

# That is another thing that the build script needs. The CI passes it so w
# Build all versions
./build-all-tor.sh

# Go back to our root repository
runcmd cd "$BUILD/../"

# For each versions, sign the generated tarball.
for file in "$VERSIONS_DIR"/*
do
    # Get version from file
    VERSION=$(basename "$file")
    TARBALL_FNAME="tor-$VERSION.tar.gz"
    CHECKSUM_FNAME="$TARBALL_FNAME.sha256sum"

    # Get checksum for that verion from the release pipeline
    URL="$GITLAB_CI_RELEASE_URL/-/jobs/artifacts/main/raw/artifacts/tarballs/$CHECKSUM_FNAME?job=maintained"
    download "$URL" "$BUILD/$CHECKSUM_FNAME"

    # Validate the checksum we created versus the one from the pipeline.
    if ! diff "$BUILD/$CHECKSUM_FNAME" "$TARBALLS_DIR/$CHECKSUM_FNAME"; then
        die "Checksum don't validate. CI has $(cat "$BUILD/$CHECKSUM_FNAME") but we have $(cat "$TARBALLS_DIR/$CHECKSUM_FNAME")"
    fi
    echo "${T_BOLD}${T_GREEN}Checksums match for $TARBALL_FNAME.${T_RESET}"

    # Tarballs signature directory
    SIG_DIR="$SIGS_DIRPATH/$VERSION"
    runcmd mkdir -p "$SIG_DIR"

    # Maybe use a specific keyid?
    if [ -n "$KEYID" ]; then
        KEYID_OPT="-u $KEYID"
    fi

    # Sign the tarball.
    # shellcheck disable=SC2086
    runcmd gpg -o "$SIG_DIR/tmp.asc" -ba $KEYID_OPT "$TARBALLS_DIR/$CHECKSUM_FNAME"
    # Get KeyID of the signed file to identify the file.
    keyid=$(gpg --list-packets "$SIG_DIR/tmp.asc" | grep "keyid" | awk '{print $NF}')
    if [ -z "$keyid" ]; then
        die "Failed to extract keyID from signed file $SIG_DIR/tmp.asc. Stopping."
    fi
    SIG_PATH="$SIG_DIR/$keyid.asc"
    runcmd mv "$SIG_DIR/tmp.asc" "$SIG_PATH"

    # Add file and "intent-to-add" so it ain't staged for commit.
    runcmd git add -N "$SIG_PATH"
    runcmd git diff "$SIG_PATH"

    while true; do
        read -rp "Commit (y/n)? " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit 0;;
            *) continue;;
        esac
    done

    # Add file for staging area and commit.
    runcmd git add "$SIG_PATH"
    runcmd git commit -S -s -m "$keyid: Signature for tor.git $VERSION"
done
